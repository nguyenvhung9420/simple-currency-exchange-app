package com.hungnguy.currencyexchange;

import android.content.SharedPreferences;
import android.os.Bundle;
import  android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import android.preference.PreferenceScreen;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings_pref);

    }
}
