//package com.hungnguy.currencyexchange;
package com.hungnguy.currencyexchange.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import android.view.View;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;

import com.hungnguy.currencyexchange.R;
import com.hungnguy.currencyexchange.SharedPreferencesClass;

public class SettingsFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SettingsViewModel settingsViewModel;
    SharedPreferences.Editor edt;
    android.widget.TextView result;
    android.widget.Button bttnChangeSettings;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {



        SharedPreferences pref = getActivity().getPreferences(Context.MODE_PRIVATE);
        edt = pref.edit();
        edt.putString("facebook_id", "Hung");
        edt.commit();

        Log.d("Shared pref in Setting ", "Hung");

        settingsViewModel =
                ViewModelProviders.of(this).get(SettingsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

//        SharedPreferences prefs = getActivity().getSharedPreferences("settings_pref", Context.MODE_PRIVATE);
//        Log.d("The prefs", prefs.toString());
//
//        Log.d("The prefs 2", prefs.getBoolean("display_text",true));
        bttnChangeSettings = root.findViewById(R.id.buttonChangeSettings);
        bttnChangeSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt.putString("facebook_id", "Pressed by Button");
                edt.commit();
            }
        });

//        final TextView textView = root.findViewById(R.id.textView);
//
//        settingsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

        return root;
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}



